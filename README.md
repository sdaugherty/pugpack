# Pugcraft 1.15 Modpack

This modpack is for the PUGS Guild Minecraft Server. It's been modified slightly from the AOF 2 modpack by AK9 to remove mods we find annoying and add ones we find useful.

It will be kept in sync with AOF-2 apart from our changes.

## Differences from AOF 2

### Added Mods

- Carpet fabric-carpet-1.15.1-1.3.1+v191212.jar
- Carpet-Extras carpet-extra-1.15-1.3.0.jar
- Minihud (minihud-fabric-1.15.1-0.19.0-dev.20191220.201948.jar)
- Stockpile (stockpile-1.1.3+1.15.1.jar)
- Tweakeroo (tweakeroo-fabric-1.15.1-0.10.0-dev.20191220.204600.jar)

### Removed Mods

- Creeper Spores
- Mouse Wheelie
- Snowdrift
- Trumpet Skeletons



# AOF 2 by AK9	
***Modpack containing the latest &amp; best Fabric mods*** \
*If you find any issues/bugs related to a specific mod, use their Issue Tracker. Only use this Issue Tracker if it's pack related.* 

**Where can you find me?** \
*All of Fabric Discord Server: https://discord.gg/6rkdm48*	

**Latest Release:** \
*MultiMC:*
https://github.com/AllOfFabric/AOF-2/releases/download/1.1.1/AOF.2.v1.1.1.zip \
**(v1.1.1)**\
*Server:*
https://github.com/AllOfFabric/AOF-2/releases/download/1.1.1/AOF.2.Server.v1.1.1.zip \
**(v1.1.1)**


**Changelogs & releases:** \
https://github.com/AllOfFabric/AOF-2/releases	


**HOW TO INSTALL MODPACK**	
1. Download MultiMC at https://multimc.org/	
2. Log into your Mojang account.	
3. Import the Zip provided above or copy the URL and paste it into MultiMC.	
4. Play & have fun! \
**Video Tutorial by SuntannedDuck:**
https://www.youtube.com/watch?v=M633Qm86tqk	

**HOW TO INSTALL SERVER**	
1. Download the All of Fabric Server files above.	
2. Extract the files to a new folder.	
3. Run Install_Mods.bat & wait for it too finish properly!
4. Run Start.cmd
5. Connect.

**Loader**
Minecraft 1.15.1
[FabricLoader 1.15.1+build.6_yarn-0.7.2+build.175](https://fabricmc.net)

**Mods**
- [Adorn: Adorn-1.6.2+1.15.1.jar](https://www.curseforge.com/minecraft/mc-mods/adorn)
- [Amecs (Fabric): amecs-1.2.9+1.15-pre5.jar](https://www.curseforge.com/minecraft/mc-mods/amecs)
- [AppleSkin: appleskin-mc1.15-fabric-1.0.8.jar](https://www.curseforge.com/minecraft/mc-mods/appleskin)
- [Aquarius: aquarius-1.4.0+1.15.jar](https://www.curseforge.com/minecraft/mc-mods/aquarius)
- [Fabric Autoswitch: autoswitch-0.4.0.jar](https://www.curseforge.com/minecraft/mc-mods/fabric-autoswitch)
- [Bee Angry-est: BeeAngryest-1.3.1+1.15.1.jar](https://www.curseforge.com/minecraft/mc-mods/bee-angry-est)
- [Bee Better: beebetter-1.6.0.jar](https://www.curseforge.com/minecraft/mc-mods/bee-better)
- [Bee-in-a-Jar: beeinajar-19w36a-1.0.0.jar](https://www.curseforge.com/minecraft/mc-mods/bee-in-a-jar)
- [Better Wither Skeletons: better-wither-skeletons-1.0.0 (1).jar](https://www.curseforge.com/minecraft/mc-mods/better-wither-skeletons)
- [Big Buckets - Fabric Edition: bigbuckets-1.0.1.jar](https://www.curseforge.com/minecraft/mc-mods/bigbuckets-fabric)
- [Bucket Hat: bucket-hat-1.0.1.jar](https://www.curseforge.com/minecraft/mc-mods/bucket-hat)
- [Climatic World Type: climaticworldtype-1.1.4.jar](https://www.curseforge.com/minecraft/mc-mods/climatic-world-type)
- Cloth
- [Command Macros: cmdkeybind-1.4.1.jar](https://www.curseforge.com/minecraft/mc-mods/command-macros)
- [Colorful Campfire Smoke: coloredcampfire-1.1.2.jar](https://www.curseforge.com/minecraft/mc-mods/colorful-campfire-smoke)
- Conveyance
- [Couplings: Couplings-1.3.1.jar](https://www.curseforge.com/minecraft/mc-mods/couplings)
- [CraftPresence: CraftPresence-Fabric-1.15.1-1.6.0.jar](https://www.curseforge.com/minecraft/mc-mods/craftpresence)
- [Creeper Spores (For Fabric): creeper-spores-1.3.1-nightly.1.15-pre3.jar](https://www.curseforge.com/minecraft/mc-mods/creeper-spores)
- [CustomTitleScreen: customtitlescreen-1.15-0.0.1.jar](https://www.curseforge.com/minecraft/mc-mods/customtitlescreen)
- [Diggus Maximus: diggusmaximus-1.2.0.jar](https://www.curseforge.com/minecraft/mc-mods/diggus-maximus)
- [Ding!: ding-3.0.0.jar](https://www.curseforge.com/minecraft/mc-mods/ding-fabric)
- [Enchanted ToolTips: EnchantedToolTips-1.2.4.jar](https://www.curseforge.com/minecraft/mc-mods/enchanted-tooltips)
- [Everything is a Hat: everything-is-a-hat-1.0.4.jar](https://www.curseforge.com/minecraft/mc-mods/everything-is-a-hat)
- [Exotic Blocks: exotic-blocks-mc115-0.13.102.jar](https://www.curseforge.com/minecraft/mc-mods/exotic-blocks)
- [Expanded Storage: expandedstorage-4.9.35+1.15.1.jar](https://www.curseforge.com/minecraft/mc-mods/expanded-storage)
- [Experience Container: Experience-Container-1.4.jar](https://www.curseforge.com/minecraft/mc-mods/experience-container)
- [Extra Pressure Plates: extra-pressure-plates-1.1.3.jar](https://www.curseforge.com/minecraft/mc-mods/extra-pressure-plates)
- [ExtraDoors: extradoors-1.3.0-1.15.jar](https://www.curseforge.com/minecraft/mc-mods/extradoors)
- [FabriBlocks: fabriblocks-1.1.7.jar](https://www.curseforge.com/minecraft/mc-mods/fabriblocks)
- [Fabric Furnaces: fabric-furnaces_1.15pre4-1.2.0.jar](https://www.curseforge.com/minecraft/mc-mods/fabric-furnaces)
- [VoxelMap: fabricmod_VoxelMap-1.9.13_for_1.15.1.jar](https://www.curseforge.com/minecraft/mc-mods/voxelmap)
- [Fat Experience Orbs: FatExperienceOrbs-0.0.6 (1).jar](https://www.curseforge.com/minecraft/mc-mods/fat-experience-orbs)
- [Flax Mod (Fabric): Flax-1.1.1.jar](https://www.curseforge.com/minecraft/mc-mods/flax-mod-fabric)
- [GamemodeOverhaul: GamemodeOverhaul-1.0.1.6.jar](https://www.curseforge.com/minecraft/mc-mods/gamemodeoverhaul)
- [Gift It! (Fabric): giftit-1.1.2+1.15-pre7.jar](https://www.curseforge.com/minecraft/mc-mods/gift-it)
- [Modern Glass Doors: glassdoor-1.4.3.jar](https://www.curseforge.com/minecraft/mc-mods/modern-glass-doors)
- [Health Overlay: HealthOverlay-1.15-2.2.10.jar](https://www.curseforge.com/minecraft/mc-mods/health-overlay)
- [Horse Stats Vanilla (Fabric): horse-stats-vanilla-1.0.3.jar](https://www.curseforge.com/minecraft/mc-mods/horsestatsvanilla)
- [Hwyla: Hwyla-fabric-1.15-pre4-1.9.19-70.jar](https://www.curseforge.com/minecraft/mc-mods/hwyla)
- [I Need Keybinds: i-need-keybinds-0.2.1-unstable.201912010804.jar](https://www.curseforge.com/minecraft/mc-mods/i-need-keybinds)
- [Inventory Profiles: inventoryprofiles-fabric-1.15-0.1.0-dev.6.jar](https://www.curseforge.com/minecraft/mc-mods/inventory-profiles)
- [Irish Wolves: irishwolves-1.0.0.jar](https://www.curseforge.com/minecraft/mc-mods/irish-wolves)
- [Iron Barrels: ironbarrels-2.1.0.jar](https://www.curseforge.com/minecraft/mc-mods/iron-barrels)
- [JumpOverFences: jumpoverfences-1.15-0.0.1.jar](https://www.curseforge.com/minecraft/mc-mods/jumpoverfences)
- [kayak: kayak-0.1.1.22.jar](https://www.curseforge.com/minecraft/mc-mods/kayak)
- [LambdaControls: lambdacontrols-1.0.1-fabric1.15.jar](https://www.curseforge.com/minecraft/mc-mods/lambdacontrols)
- [Light Overlay (Rift/Forge/Fabric): LightOverlay-4.1.jar](https://www.curseforge.com/minecraft/mc-mods/light-overlay)
- [Lil Tater: liltater-1.1.0.jar](https://www.curseforge.com/minecraft/mc-mods/lil-tater)
- Malilib
- [Materialisation: Materialisation-2.0.0-alpha.3.jar](https://www.curseforge.com/minecraft/mc-mods/materialisation)
- [Mod Menu (Fabric): modmenu-1.8.1+build.17.jar](https://www.curseforge.com/minecraft/mc-mods/modmenu)
- [More Enchantments: MoEnchantments-1.8.2.jar](https://www.curseforge.com/minecraft/mc-mods/fabric-more-enchantments)
- [More Berries: moreberries-1.2.3.jar](https://www.curseforge.com/minecraft/mc-mods/more-berries)
- [Mouse Wheelie (Fabric): mousewheelie-1.3.10+1.15.1-pre1.jar](https://www.curseforge.com/minecraft/mc-mods/mouse-wheelie)
- [Ok Zoomer: okzoomer-1.0.2.jar](https://www.curseforge.com/minecraft/mc-mods/ok-zoomer)
- [Phosphor: phosphor-mc1.15.1-fabric-0.3.3.jar](https://www.curseforge.com/minecraft/mc-mods/phosphor)
- [Redstone Bits: redstone-bits-1.2.2.jar](https://www.curseforge.com/minecraft/mc-mods/redstone-bits)
- [RetroExchange: Retro-Exchange-1.15-3.1.0.jar](https://www.curseforge.com/minecraft/mc-mods/retroexchange)
- [Right Click Clear: right-click-clear-1.1.1-unstable.201912010707.jar](https://www.curseforge.com/minecraft/mc-mods/right-click-clear)
- [Roughly Enough Items: RoughlyEnoughItems-3.2.28.jar](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items)
- [Screenshot to Clipboard (Fabric): ScreenshotToClipboardFabric-1.0.3.jar](https://www.curseforge.com/minecraft/mc-mods/screenshot-to-clipboard-fabric)
- [Shulker Charm: shulkercharm-1.0.1+1.15.jar](https://www.curseforge.com/minecraft/mc-mods/shulker-charm)
- [Simple Teleporters (Fabric): simpleteleporters-2.2.0-1.15.jar](https://www.curseforge.com/minecraft/mc-mods/simple-teleporters-fabric)
- [Simple Void World: SimpleVoidWorld-3.1.0.jar](https://www.curseforge.com/minecraft/mc-mods/simple-void-world)
- [Simplex Terrain Generation: simplexterrain-0.4.3.jar](https://www.curseforge.com/minecraft/mc-mods/simplex-terrain-generation)
- [Sit (Fabric): sit-1.15-4.jar](https://www.curseforge.com/minecraft/mc-mods/sit-fabric)
- [Smooth Scrolling Everywhere (Fabric): SmoothScrollingEverywhere-2.2.1-unstable.201912010747.jar](https://www.curseforge.com/minecraft/mc-mods/smooth-scrolling-everywhere-fabric)
- [Snowdrift: snowdrift-1.0.1.jar](https://www.curseforge.com/minecraft/mc-mods/snowdrift)
- [Sweed: Sweed-1.1.1.jar](https://www.curseforge.com/minecraft/mc-mods/sweed)
- [Sweet Tooth: sweet-tooth-1.1.2-1.15.jar](https://www.curseforge.com/minecraft/mc-mods/sweet-tooth)
- [Trees Do Not Float: tdnf-mc115-1.8.69.jar](https://www.curseforge.com/minecraft/mc-mods/trees-do-not-float)
- [Tech Reborn: TechReborn-1.15-3.1.0+build.59.jar](https://www.curseforge.com/minecraft/mc-mods/techreborn)
- [Terrestria (Fabric): terrestria-1.1.1+build.18.jar](https://www.curseforge.com/minecraft/mc-mods/terrestria)
- Thehallow
- [Torcherino: Torcherino-2.11.58.jar](https://www.curseforge.com/minecraft/mc-mods/torcherino)
- [Traverse (Fabric): traverse-2.1.4+build.10.jar](https://www.curseforge.com/minecraft/mc-mods/traverse)
- [Trumpet Skeleton (Fabric): trumpet-skeleton-fabric-1.1.1.jar](https://www.curseforge.com/minecraft/mc-mods/trumpet-skeleton-fabric)
- [VariablePaxels: VariablePaxels-1.15-1.0.2.jar](https://www.curseforge.com/minecraft/mc-mods/variablepaxels)
- [Vertical Redstone: vertical-redstone-1.1.0.jar](https://www.curseforge.com/minecraft/mc-mods/vertical-redstone)
- [Wild World: WildWorld-1.2.0+1.15.jar](https://www.curseforge.com/minecraft/mc-mods/wild-world)
- [Wrench Anything: wrenchanything-1.1.2.jar](https://www.curseforge.com/minecraft/mc-mods/wrench-anything)
- [Yet Another Gravestone Mod: yet-another-gravestone-mod-1.1.0.jar](https://www.curseforge.com/minecraft/mc-mods/yet-another-gravestone-mod)

**APIs/LIBs**
- [Cotton: cotton-1.0.0-rc.6.jar](https://www.curseforge.com/minecraft/mc-mods/cotton)
- [Exotic Matter Library: exotic-matter-mc115-1.13.283.jar](https://www.curseforge.com/minecraft/mc-mods/exotic-matter-library)
- [Fabric API: fabric-api-0.4.25+build.282-1.15.jar](https://www.curseforge.com/minecraft/mc-mods/fabric-api)
- [Fabric Language Kotlin: fabric-language-kotlin-1.3.61+build.1.jar](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin)
- [Reborn Core: RebornCore-1.15-4.0.28+build.48.jar](https://www.curseforge.com/minecraft/mc-mods/reborncore)
- [Towelette: Towelette-2.1.3+1.15.jar](https://www.curseforge.com/minecraft/mc-mods/towelette)
- [Trinkets (Fabric): trinkets-2.3.0.jar](https://www.curseforge.com/minecraft/mc-mods/trinkets-fabric)
- [Wrenchable: wrenchable-0.2.1.jar](https://www.curseforge.com/minecraft/mc-mods/wrenchable)
